import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.Date;

/**
 * Created by Geza-Czimeth on 15/04/2014.
 */
@Path("/customer")
@Component
public class PrintService {

    @Autowired
    Course course;

    public PrintService(){
        System.out.println("creating printservice");
    }
    @GET
    @Path("/print")
    public String printMessage() {
        return "success" + course.toString() + ", "+ new Date();

    }

}