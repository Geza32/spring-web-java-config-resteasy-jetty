
import justforfun.MyClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "")
public class MyAppContext {

    public MyAppContext()
    {
        System.out.println("created");
    }
    @Bean(initMethod = "setup", destroyMethod = "cleanup")
    public Course course() {
        Course course = new Course();
     //   course.setModule(module());
        return course;
    }

    @Bean
    public MyClass getMyClas() {
       return new MyClass();
    }
}	