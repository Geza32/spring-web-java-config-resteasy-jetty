package justforfun;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by Geza-Czimeth on 15/04/2014.
 */
@Path("/rest")
@Component

public class JustAnotherEndpoint {

    @Autowired
    public MyClass clazz;

    @POST
    @Path("/testPost")
    //@Consumes(MediaType.TEXT_PLAIN)
   /* @Produces("text/plain")*/
    public String doTest(String content)
    {
        System.out.println(content);
        return "Post passed!";
    }
    @PUT
    @Path("/testPut")
    public String doPutTest()
    {
        return "put passed!";
    }
}
