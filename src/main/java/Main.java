import org.eclipse.jetty.server.Server;

import org.eclipse.jetty.webapp.WebAppContext;
import org.jboss.resteasy.plugins.spring.SpringContextLoaderListener;

/**
 * Created by Geza-Czimeth on 15/04/2014.
 */
public class Main {

    public static void main(String args[]) throws Exception {
        Server server = new Server( 8080 );
        System.setProperty("org.eclipse.jetty.LEVEL", "DEBUG");
        WebAppContext root = new WebAppContext();

        //root.setDescriptor("f:/active/intellijws/src/main/resources/webapp/WEB-INF/web.xml");
        //root.setResourceBase("f:/active/intellijws/src/main/resources/webapp");

        root.setDescriptor("src/main/resources/webapp/WEB-INF/web.xml");
        root.setResourceBase("src/main/resources/webapp");

        root.setServer(server);
        server.setHandler(root);
        server.start();
        server.join();

//http://stackoverflow.com/questions/4390093/add-web-application-context-to-jetty
        //context.setResourceBase("/path-to-your-project/WebContent");
       // context.setDescriptor("/path-to-your-project/WebContent/WEB-INF/web.xml");
    }
}
